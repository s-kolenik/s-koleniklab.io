# Summary

[Introduction](./introduction.md)

# Notes

- [Bash](./bash.md)
  - [Arch Linux](./arch-linux.md)
  - [Networking](./networking.md)
- [Editory a poznámky]()
  - [Emacs](./emacs.md)
- [Git](./git.md)
- [Průzkumníci souborů](./file-managers.md)
- [Synchronizace a zálohování](./sync-and-backup.md)
- [Latex]()
- [Gnuplot]()
- [i3]()

[Draft 2]()
