# Arch Linux
[Wiki](https://wiki.archlinux.org/)

## Pacman

- `sudo pacman -S xyz` - nainstalovat balíček 
- `sudo pacman -R xyz`  - odinstalovat balíček
- `sudo pacman -Rs xyz` - odinstalovat balíček a všechny nepotřebné závislosti
- `sudo pacman -Sy`     - update databáze
- `sudo pacman -Su`     - upgrade
- `sudo pacman -Sy archlinux-keyring && pacman -Su` - upgdate a upgrade po dlouhé době
- `pacman -Ss xyz`      - hledat balíček
- `sudo pacman-key --refresh-keys` - v případě chyby podpisů
- `pacman -Q xyz`       - zjistit verzi nainstalovaného balíčku