# Bash a související utility

### find
- Exec pro každý soubor zvlášť:
  ```
  $ find . -name "*.txt" -exec grep "something" {} \;
  ```

- Exec pro všechny soubory najednou:
  ```
  $ find . -name "*.txt" -exec grep "something" {} +
  ```