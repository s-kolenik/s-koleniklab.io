# Emacs

## Nastavení

### Windows
Oficiální [download stránky](https://www.gnu.org/software/emacs/download.html) odkazují na hlavní [FTP server](http://ftp.gnu.org/gnu/emacs/windows/) GNU.
Odtud lze stáhnout ZIP (např. `emacs-29.1_2.zip`), který obsahuje všechny soubory a není jej nutné instalovat.

Mezi soubory ve složce `bin` je jednak `emacs.exe`, ale také `runemacs.exe`. `emacs.exe` lze spustit jak z GUI, tak z CLI a vždy spustí správnou verzi programu.
Při spouštění z GUI ale vytvoří navíc CLI okno, což může být ošklivé. `runemacs.exe` je čistě GUI a je tudíž vhodnější například pro připnutí na hlavní panel.
Pak ale skutečná okna emacs nejsou pod tuto položku seskupena, protože se liší název. Je třeba okno emacs *Připnout na hlavní panel*.
Následně jít do nastavení tohoto zástupce a změnit cíl z `...\bin\emacs.exe` na `...\bin\runemacs.exe`. Pak funguje připnutí na hlavní panel bez problémů.
([zdroj](https://lists.gnu.org/archive/html/help-gnu-emacs/2004-03/msg00168.html), [zdroj](https://sachachua.com/blog/2012/06/making-gnu-emacs-play-well-on-microsoft-windows-7/) Step 5)

### Init soubor `.emacs`
Umístění init souboru lze přímo z emacs nalézt v popisu proměnné `user-init-file`. Nápovědu k libovolné proměnné lze vyhledat pomocí `C-h v`.
Pro rychlé otevření init souboru lze použít `M-:` (příkaz `eval-expression`) a vepsat příkaz `(find-file user-init-file)`.
- Na Windows bývá init soubor umístěn v `AppData/Roaming/.emacs`.

Možná podoba init souboru:
```
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(calendar-week-start-day 1)
 '(org-agenda-files '("p:/org/notes.org"))
 '(org-hide-emphasis-markers t)
 '(org-special-ctrl-a/e t)
 '(visible-bell t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; Hide welcome screen
(setq inhibit-splash-screen t)

;; No blank line when inserting new heading/listitem
(setf org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))

;; Limit text width in org-mode, use org-indent-mode
(add-hook 'org-mode-hook 'turn-on-auto-fill)
(add-hook 'org-mode-hook 'org-indent-mode)

;; Ignore workspace changing keys - prevent oragne task bar icon
(global-set-key (kbd "<C-lwindow>") 'ignore)
(global-set-key (kbd "<C-rwindow>") 'ignore)

;; Make Ctrl+z do the same thing as Ctrl+y, that is paste text instead
;; of freezing the whole program
(global-set-key (kbd "C-z") (kbd "C-y"))

;; Bookmark list shortcut
(global-set-key (kbd "C-c b") 'bookmark-bmenu-list)

;; Tmp files folder
(setq backup-directory-alist `((".*" . "~/.emacs-tmp-files/")))

;; WAIT todo keyword color
;; to use WAIT add this at the beginning of the org file:
;;   #+TODO: TODO(t) WAIT(w) | DONE(d)
(setq org-todo-keyword-faces '(("WAIT" . "orange2")))
```
