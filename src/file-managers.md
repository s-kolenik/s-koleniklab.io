# Průzkumníci souborů

## Total Commander

### Nastavení
- Vyhledávání souborů ve složce psaním: v menu `Configuration -> Options... -> Quick Search -> Letter only`.
- Přejmenovat bez koncovky: v menu `Configuration -> Options... -> Operation -> Select only the file name when renaming`.
- Klávesové zkratky: v menu `Configuration -> Options... -> Misc. -> Redefine hotkeys`.
  - `Alt+J` = `cm_GoToNext`
  - `Alt+K` = `cm_GoToPrev`
  - `Alt+L` = `cm_GoToDir`
  - `Alt+H` = `cm_GoToParent`

### Klávesové zkratky
- Přejmenování aktuální položky: `Shift+F6`
- Zkopírování aktuální položky: `Shift+F5`
- Otevření nového tabu: `Ctrl+T`