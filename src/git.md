# Git, GitLab, GitHub

### Generating SSH keys

- Run this command:
  
  ```
  $ ssh-keygen -t rsa -b 4096 -C "<COMMENT>"
  ```
  
  String `<COMMENT>` is appended at the end of the public key. In none is specified, local username and hostname are used.

- Enter file in which to save the key or press `enter` for default. Enter passphrase or press `enter` for no passphrase.

### Managing multiple SSH keys

Create the file `~/.ssh/config` with contents similar to this:

```
Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/git-personal
  IdentitiesOnly yes

Host *
  IdentityFile ~/.ssh/id_rsa
```

Now, run `kill $(pgrep ssh-agent) && ssh-agent` to reload the configuration.

[Source](https://www.freecodecamp.org/news/how-to-manage-multiple-ssh-keys/)

### Finding SSH key length

For private key, run:

```
$ openssl rsa -in <PRIV_KEY_PATH> -text -noout | grep "Private-Key"
```

For public key, run:

```
$ ssh-keygen -lf <PUB_KEY_PATH>
```

[Source](https://stackoverflow.com/questions/56827341/how-to-know-the-ssh-keys-length)

### Git commands

- List remotes:
  
  ```
  $ git remote -v
  ```

- Change remote url:
  
  ```
  $ git remote set-url <REMOTE_NAME> <REMOTE_URL>
  ```

- First time branch push:
  
  ```
  $ git push -u origin <BRANCH_NAME>
  ```
  
  The `-u` option sets up an "upstream" (tracking) reference. This enables argument-less `git push` and `git pull` in the future.

- Checkout remote branch:
  
  ```
  $ git switch -c <BRANCH_NAME> <REMOTE_NAME>/<BRANCH_NAME>
  ```
  
  Or simply `git switch <BRANCH_NAME>` if there is only one remote.

- Set commit name and email:
  
  ```
  $ git config user.name  "<NAME>"
  $ git config user.email "<EMAIL>"
  ```
  
  This configures the identity only for the current repository, add `--global` to change the global identity. Use `$ git config --list` to see the changes, add `--show-origin` to see from which file each setting is sourced.

#### Submoduly
- Inicializace submodulů po naklonování repozitáře:

  ```
  $ git submodule update --init --recursive
  ```

- Odebrání submodulu:

  ```
  $ git rm <PATH_TO_SUBMODULE>
  ```

  Ponechá nějaké soubory v `.git`, aby bylo možné provést checkout předchozích commitů. Viz [SO](https://stackoverflow.com/a/1260982).
