# Networking

### Private IP ranges

The private IP address ranges are `10.0.0.0/8`, `172.16.0.0/12` and `192.168.0.0/16`.

### Basic configuration

Enable the device `<DEV>`:

```
# ip link set <DEV> up
```

Set IP address:

```
# ip addr add <ADDR>/<MASK_LEN> dev <DEV>
```

The `addr` part can be shortened to just `a`. As in `ip a` to show addresses assigned to all network interfaces.

### Routing table

To print the routing table use one of the following commands:

```
$ ip route show
$ ip r
```

Add a default gateway:

```
# ip route add default via <ADDR> dev <DEV>
```

### DNS

Doménová jména mohou být čerpána z několika zdrojů. Jejich pořadí je určeno v `/etc/nsswitch.conf`, např:
```
hosts: mymachines resolve [!UNAVAIL=return] files myhostname dns
```
- `files` vede na soubor `/etc/hosts`
- `dns` vede na soubor `/etc/resolv.conf`
- `resolve` vede na službu `systemd-resolved` a její konfigurační soubor `/etc/systemd/resolved.conf`
- `mymachines` a `myhostname` vedou na jiné systemd služby

Viz Arch Wiki: [Domain name resolution](https://wiki.archlinux.org/title/Domain_name_resolution).

Přidat DNS server tedy lze např. takto:

```
# neprivilegovaný uživatel
echo "nameserver 1.1.1.1" | sudo tee -a /etc/resolv.conf

# jako root
echo "nameserver 1.1.1.1" >>/etc/resolv.conf
```