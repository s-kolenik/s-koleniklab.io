# Synchronizace a zálohování

## Rclone

### Nastavení
- Nastavení remotů:
  ```
  $ rclone config
  ```
  Umožňuje interaktivně vytvářet, editovat i odebírat remoty. Nastavení jsou uložena do konfiguračního souboru. Jeho umístění lze zjistit pomocí `rclone config file`.

### Získávání informací
- Vypsání remotů spolu s jejich typem:
  ```
  $ rclone listremotes --long
  ```

- Vypsání všech souborů:
  ```
  $ rclone ls <REMOTE>:<PATH>
  ```
  Výchozím chováním je rekurzivní, lze omezit pomocí `--max-depth 1`. Vypisuje ale pouze soubory, nikoliv složky.
  Naopak pro vypsání pouze složek sloužít `rclone lsd <REMOTE>:<PATH>`, to je pro změnu nerekurzivní (lze vynutit pomocí `-R`).

- Nápověda a verze:
  ```
  $ rclone --help
  $ rclone <COMMAND> --help
  $ rclone version
  ```

### Použití